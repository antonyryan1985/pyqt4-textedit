from os import path
from setuptools import (setup, find_packages)

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'readme.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="textedit",
    version="0.1.0",
    description="Simple text editor",
    long_description=long_description,
    long_description_content_type="text/markdown",
    #The PyQt5/Python3.7 rewrite was for Antony Ryan's benefit
    url="https://gitlab.com/antonyryan1985/pyqt4-textedit",
    author="Antony Ryan",
    # https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Text Editors',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='texteditor speechrecognition pyqt5',
    packages=find_packages(exclude=['icons', 'doc', 'dist']),
    python_requires='>=3.7',
    install_requires=['mammoth>=1.4.9', 'PyAudio>=0.2.11', 'SpeechRecognition>=3.8.1'],
    
    include_package_data=True,
    exclude_package_data={'': ['icons/icons.rar']},
    # https://setuptools.readthedocs.io/en/latest/setuptools.html#including-data-files
    package_data={
        '': ['*.ini', '.gitlab-ci.yml', 'icons.qrc', 'readme.md', 'icons/*.png', 'icons/*.svg', 'icons/readme.md']
    },
    #Automatically discover unittests in test folder
    #test_suite = 'test',
    entry_points={
        'console_scripts': [
            'textedit = textedit.__main__:main',
        ]
    }
)
